"use strict";

const http = require("http");
const express = require("express");
const WsServer = require("ws").Server;

var defaults = {
    "ws": true
};

function noOp() {} // eslint-disable-line no-empty-function

function expressWs(options) {
    let opts = Object.assign({}, defaults, options);
    let that = express();

    that._ws = Boolean(opts.ws);
    that._wss = null;
    that._http = null;

    Object.defineProperty(that, "wsServer", {
        "set": function set() {
            throw new Error("wsServer is a read-only value");
        },
        "get": function get() {
            return that._wss;
        },
        "configurable": false,
        "enumerable": true
    });

    Object.defineProperty(that, "httpServer", {
        "set": function set() {
            throw new Error("httpServer is a read-only value");
        },
        "get": function get() {
            return that._http;
        },
        "configurable": false,
        "enumerable": true
    });

    Object.defineProperty(that, "listen", {
        "value": function value() {
            function onWsUpgrade(req, sock, head) {
                var res = new http.ServerResponse(req);

                res.assignSocket(sock);
                res.on("finish", function onFinish() {
                    res.socket.destroy();
                });
                res.upgradeWs = function upgradeWs(cb) {
                    that._wss.handleUpgrade(req, sock, head, function onUpgrade(ws) {
                        if (cb) {
                            cb(ws);
                        }
                    });
                };
                that(req, res);
            }
            that._http = http.createServer(that);
            if (that._ws) {
                that._wss = new WsServer({
                    "noServer": true,
                    "clientTracking": true
                });
                that._http.on("upgrade", onWsUpgrade);
            }
            that._http.listen.apply(that._http, arguments);
        },
        "writable": false,
        "configurable": false,
        "enumerable": true
    });

    Object.defineProperty(that, "close", {
        "value": function value(callback) {
            var wssErr;

            if (!that._http) {
                throw new Error("server not started");
            }
            if (that._ws) {
                that._wss.close(function onClose(err) {
                    wssErr = err;
                    that._wss = null;
                });
            }
            that._http.close(function onClose(httpErr) {
                that._http = null;

                return callback ? callback(httpErr || wssErr) : noOp();
            });
        },
        "writable": false,
        "configurable": false,
        "enumerable": true
    });

    return that;
}

module.exports = expressWs;
