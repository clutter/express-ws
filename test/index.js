/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const express = require("express");
const expressWs = require("../");
const request = require("supertest");
const WebSocket = require("ws");

const app = express();
const appWs = expressWs();

// setup middleware
appWs.get("/parent", function (req, res) {
    res.send("parent");
});

app.get("/child", function (req, res) {
    res.send("child");
});

app.get("/ws-deny", function (req, res) {
    res.sendStatus(401);
});

app.post("/ws", function (req, res) {
    res.send("ws");
});

app.get("/ws", function (req, res) {
    res.upgradeWs(function (ws) {
        if (!ws) {
            return;
        }
        ws.on("message", function (data) { // loopback
            ws.send(data);
        });
    });
});

appWs.use(app);

// tests start here
before(function (done) {
    appWs.listen(8080, done);
});

describe("ExpressWs App", function () {
    it("should export function", function (done) {
        assert.strictEqual(typeof expressWs, "function");
        done();
    });
    it("should work for child-app", function (done) {
        request(appWs)
            .get("/child")
            .expect(200)
            .expect("child", done);
    });
    it("should work for parent-app", function (done) {
        request(appWs)
            .get("/parent")
            .expect(200)
            .expect("parent", done);
    });
    it("should allow other methods than GET for ws-path", function (done) {
        request(appWs)
            .post("/ws")
            .expect(200)
            .expect("ws", done);
    });
    it("should support GET with WS-upgrade", function (done) {
        var msg = "hello world",
            ws = new WebSocket("ws://localhost:8080/ws");

        ws.on("message", function (data) {
            assert.equal(data, msg);
            ws.close();
        });
        ws.on("open", function () {
            ws.send(msg);
        });
        ws.on("close", function () {
            done();
        });
    });
    it("should support denying GET with WS-upgrade", function (done) {
        var ws = new WebSocket("ws://localhost:8080/ws-deny");

        ws.on("unexpected-response", function (req, res) {
            assert.equal(res.statusCode, 401);
            done();
        });
    });
    it("should provide read-only wsServer & httpServer as attributes", function (done) {
        assert.ok(appWs.httpServer);
        assert.throws(function () {
            appWs.httpServer = null;
        }, Error);
        done();
        assert.ok(appWs.wsServer);
        assert.throws(function () {
            appWs.wsServer = null;
        }, Error);
    });
    it("should close ws-connections on close", function (done) {
        var idx;
        var ws;
        var closeCount = 0;
        var openCount = 0;

        function onClose() {
            closeCount++;
            if (closeCount === idx) {
                done();
            }
        }

        function onOpen() {
            openCount++;
            if (openCount === idx) {
                assert.strictEqual(appWs.wsServer.clients.size, openCount);
                appWs.wsServer.clients.forEach(function (sock) {
                    sock.on("close", onClose);
                });
                appWs.close();
            }
        }

        for (idx = 0; idx < 10; idx++) {
            ws = new WebSocket("ws://localhost:8080/ws");
            ws.on("open", onOpen);
        }
    });
});

after(function (done) {
    done();
});
